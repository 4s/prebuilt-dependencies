Prebuilt dependencies
=====================

This repository contains prebuilt libraries used for building the
PHG Core. All library x target combinations are stored on individual
branches.

The following sections lists the libraries, their license terms, and
how to acquire the source code:


Google Protocol Buffers
-----------------------

 - https://developers.google.com/protocol-buffers/
 - https://github.com/protocolbuffers

### License

3-clause BSD License


ZeroMQ / JeroMQ
---------------

 - https://zeromq.org/
 - https://github.com/zeromq

### License

MIT License (ZeroMQ) and MPL 2.0 (JeroMQ)

